/**
 * Created by bou on 11/29/2015.
 */

module.exports = function(grunt) {

    grunt.initConfig({

        nodemon: {
            dev: {
                script: 'webapp/server.js'
            }
        }

    });

    // load nodemon
    grunt.loadNpmTasks('grunt-nodemon');

    // register the nodemon task when we run grunt
    grunt.registerTask('default', ['nodemon']);

};