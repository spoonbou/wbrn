/**
 * Created by bou on 11/29/2015.
 */

var express = require('express');
var app     = express();

//app.use(express.static(__dirname + '/webapp'));

app.get('/', function(req, res) {
    res.sendfile('webapp/index.html');
});

app.listen(8000);
console.log('Magic happens on 8000');
